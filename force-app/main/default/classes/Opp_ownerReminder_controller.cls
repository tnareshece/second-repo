public class Opp_ownerReminder_controller{

@future(callout=false)
public static void addOPP(Map<ID,ID>mapowner){
List<opportunity>oppval=[select id,stagename,OwnerId,accountid from opportunity where stagename = 'closed won' AND id in:mapowner.keySet()];
for(opportunity o:oppval){
o.ownerid=mapowner.get(o.id);
}
update oppval;
}

}