public with sharing class ImagInsertionIntoRichText {

    /*public String FileId { get; set; }

    public String con { get; set; }

    public PageReference insrt() {
        return null;
    }


    public String file { get; set; }

    public String textName { get; set; }
}

{*/

public string FileId { get; set; }
public String getFileId;
public boolean con { get; set; }
public String textName{get;set;}
public ID folderid{get;set;}
public Blob file{get;set;}

public PageReference insrt()
{
Document d= new Document();
d.name = textName;
d.body=file; // body field in document object which holds the file.
d.folderid='00l90000001SuJs'; //folderid where the document will be stored insert d;

insert d;
con=true;

List<Document> attachedFiles = [select d.Id,d.body,d.Name from Document d order by Id Desc limit 1];
if( attachedFiles != null && attachedFiles.size() > 0 ) {
fileId = attachedFiles[0].Id;

}
return null;

}
}