public class wrapperClassControllerTEST {

Public List<cContact> contactList {get; set;}

public List<cContact> getContacts() {

if(contactList == null) {

contactList = new List<cContact>();

for(Contact c: [select Id, Name, Email, Phone from Contact limit 10]) {

 contactList.add(new cContact(c));

}
}

return contactList;

 }

Public Pagereference save() {

List<Contact> Cs = new List<Contact>();

for(cContact cCon: getContacts()) {

Cs.add(cCon.con);

}

update Cs;

contactList = null;

getContacts();

return null;

}

public PageReference processSelected() {

List<Contact> selectedContacts = new List<Contact>();

for(cContact cCon: getContacts()) {

if(cCon.selected == true) {

selectedContacts.add(cCon.con);
}

}

System.debug('These are the selected Contacts...');

for(Contact con: selectedContacts) {

system.debug(con);

}

contactList=null; 
// we need this line if we performed a write operation  because getContacts gets a fresh list now

return null;

}

// This is our wrapper/container class. A container class is a class, a data structure, or an abstract data type whose instances are collections of other objects. In this example a wrapper class contains both the standard salesforce object Contact and a Boolean value

public class cContact {

public Contact con {get; set;}

public Boolean selected {get; set;}

public cContact(Contact c) {

 con = c;

selected = false;

}

}

}