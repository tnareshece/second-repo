public class rect_vf2 implements RectIF{
  //data members
        integer num1;
        integer num2;
    integer res;
    
        // member methods
    //create
    public Rect_Vf2(){
        
    }
    //input
    public integer setnum1(integer n1){
       num1=n1;
        return n1;
       }
    public integer setnum2(integer n2){
       num2=n2;
        return n2;
       }
    public integer setres(integer r){
       res=r;
        return r;
       }
       
    //process
    public double area(){
        res=num1*num2;
        return res;
    }
    
    //output
    public integer getnum1(){
    return(num1);
        
    }
     public integer getnum2(){
    return(num2);
  }
 public integer getres(){
    return(res);
}

}