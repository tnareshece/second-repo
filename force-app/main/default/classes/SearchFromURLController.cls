public with sharing class SearchFromURLController {

    public String executeSearch { get; set; }

    public List<Account> accounts { get; set; }

    public Boolean searched { get; set; }
    
    public SearchFromURLController(){
    
    Searched=False;
    String nameStr=Apexpages.currentpage().getParameters().get('name');
    if(null!=nameStr)
    {
     name=nameStr;
     executeSearch();
    
    }
    
    
    }
    public PageReference executeSearch() {
       Searched=true;
       String searchStr = '%'+ name + '%';
       accounts = [Select id,Name,Industry,Type FROM Account WHERE name LIKE:searchStr];
       
       return null;
    }


    public String name { get; set; }
}