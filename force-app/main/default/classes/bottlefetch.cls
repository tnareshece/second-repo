public with sharing class bottlefetch {

    public Integer quantity { get; set; }

    public Decimal price { get; set; }

    public String bname { get; set; }

    public PageReference save() {
        bottle__c btl = new bottle__c();
        btl = [select name,price__c from bottle__c limit 1 offset 2];
        bname=btl.name;
        price=btl.price__c;

        return null;
    }

}