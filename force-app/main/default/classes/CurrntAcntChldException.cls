public class CurrntAcntChldException extends SavingsBankAcntChldException {
     //data mem
     
     //mem meth
     public CurrntAcntChldException(Integer an, Integer ba, String cn, Integer mb){
        account_num=an;
        balance=ba;
        customer_name=cn;
        min_bal=mb;
     }
     
     public override void withdraw(Integer wtd){
     withdraw=wtd;
        System.debug('withdr amnt is = '+withdraw);
        try{
            if( balance<=withdraw || balance<min_bal){
                 throw new BankAcntPrtException();
             }
            else{
                 balance-=withdraw;
                 System.debug('available balnce is = '+balance);
             }
           } 
        catch(BankAcntPrtException e){
                balance-=min_bal;
               System.debug('Insufficient Balance in ur current acnt');
               System.debug('Minimum balance charges are deducted & current balance is'+balance);
              }
     
     }
}