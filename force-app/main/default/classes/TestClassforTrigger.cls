@isTest(seeAllData=false)
private class TestClassforTrigger {

 static testMethod void Task() {
         
        Lead ld = new Lead();
        ld.LastName = 'Tulsani';
        ld.Company ='ABC Corp';
       ld.Status='Unqualified';
        insert ld;
        
        Task tk = new Task();
        tk.Subject ='Call for test class';
        tk.WhoId = ld.id;
        tk.Priority = 'Normal';
        tk.status='Inprogress';
        insert tk;
   
        test.startTest();

        ld.Status = 'Disqualified';
        update ld;
        if(ld.status=='Disqualified'){
        tk.status = 'Completed';
        tk.Description = 'Auto-Closed by the SalesOps Trigger DisqStatusLeadTaskUpdate Disqualified or Return To Marketing Stage';
        update tk;
        }
        
        test.stopTest(); 
  }
  
  static testMethod void Task2() {
         
        Lead ld = new Lead();
        
        ld.LastName = 'Tulsani';
        ld.Company ='ABC Corp';
        ld.Status='Unqualified';
        insert ld;
        
        Task tk = new Task();
        tk.Subject ='Call for test class';
        tk.WhoId = ld.id;
        tk.Priority = 'Normal';
        tk.status='Inprogress';
        insert tk;
   
        test.startTest();

        ld.Status = 'Disqualified';
        update ld;
        if(ld.status=='Disqualified'){
        tk.status = 'Completed';
        tk.Description = 'Auto-Closed by the SalesOps Trigger DisqStatusLeadTaskUpdate Nurture Stage';
        update tk;
        }
        test.stopTest(); 
  
    }
    
 }