public virtual class SavingsBankAcntChldException extends BankAcntPrtException {
    //data mem
    Protected Integer min_bal;
    
    //mem meth
    //create
    //PC
    public SavingsBankAcntChldException(Integer min_bal){
        super(7777,5050,'NK');
        this.min_bal=min_bal;
     }
    
    //input
     public void setValues(Integer min_bal){
        super.setValues(444,4040,'NK2');
        this.min_bal=min_bal;
        }
            
    public virtual override void withdraw(Integer Wtd){
        withdraw=wtd;
        System.debug('withdr amnt is = '+withdraw);
        try{
            if( balance<=withdraw || balance<min_bal){
                 throw new BankAcntPrtException();
             }
            else{
                 balance-=withdraw;
                 System.debug('available balnce is = '+balance);
             }
           } 
        catch(BankAcntPrtException e){
                balance-=50;
               System.debug('Insufficient Balance');
               System.debug('Minimum balance charges are deducted & current balance is'+balance);
              }
     }
      public override void display(){
       System.debug('Account Number =' +account_num); 
        System.debug('Balance =' +balance); 
        System.debug('Customer Name =' +customer_name);
          
         System.debug('Minimum Bal =' +min_bal);   
         
      }




}