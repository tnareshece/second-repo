public class OpportunityLVs {

    public String opportunity { get; set; }

    //public String Opportunities { get; set; }
//http://www.jitendrazaa.com/blog/salesforce/listview-filter-in-apex-with-paging-and-navigation/
//https://developer.salesforce.com/forums/?id=906F00000008zGlIAI
    public Opportunity opps { get; set; }
    
      private ApexPages.StandardSetController ssc;

    public OpportunityLVs() 
    {
        List<Opportunity> caseList = [SELECT Name FROM Opportunity];

        ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(caseList);
        ssc.setFilterID('00B90000007NVIC');
    }

    public List<Opportunity> getopps() 
    {
        return (List<Opportunity>)ssc.getRecords();
    }
}