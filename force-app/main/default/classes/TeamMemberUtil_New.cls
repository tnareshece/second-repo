public with sharing class TeamMemberUtil_New {
 public static void OpportunityTeamMembers( map<ID,String> OpportunityTeam,map<ID,String> OpportunityTeamRole){
system.debug('---OpportunityTeam---'+OpportunityTeam);
 system.debug('---OpportunityTeamRole---'+OpportunityTeamRole);
     List<OpportunityTeamMember> members = new list<OpportunityTeamMember>();
    
    for(id ownerID : OpportunityTeam.keyset()) {
        ID oaccID = OpportunityTeam.get(ownerID);//OPP ID
      members.add(New OpportunityTeamMember(OpportunityId = oaccID, TeamMemberRole = OpportunityTeamRole.get(oaccID), UserId= ownerID));
    }
       if(!members.isEmpty()){
       Schema.DescribeSObjectResult opteamSObj = Schema.sObjectType.Opportunity;

       if(opteamSObj.iscreateable()){
        try{

        insert members;
        system.debug('---members---'+members);
        
        }
        catch(Exception e){
        system.debug('---members---'+e);
        }
       
       }
     }
     
    }
}