public class Point{
 //data members
    integer xab1;
    integer yab1;
    integer xab2;
    integer yab2;
    
    //member methods
    //create
    public point(){
        xab1=0;
        yab1=0;
        xab2=0;    
        yab2=0;    
    }
    //input
    public void setvalues(integer x1, integer y1, integer x2, integer y2){
        xab1=x1;
        yab1=y1;
        xab2=x2;    
        yab2=y2;  
    }     
    //process
    public double Slope(){
        double sl;
        sl=(yab2-yab1)/(xab2-xab1);
        return sl;
    }
    //output
    public void getvalues(){
        system.debug('The points are ('+xab1+','+yab1+') ('+xab2+','+yab2+')');
    }
    
}