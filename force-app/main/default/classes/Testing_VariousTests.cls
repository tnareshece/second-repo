@isTest(SeeAllData=true)
public class Testing_VariousTests {

    static testmethod void TestEmailMessage() {
    
    
        List<EmailMessage> msgs = [SELECT Id, ParentId, FromAddress, ToAddress, CcAddress, BccAddress, FromName, Subject, TextBody, HtmlBody, Status, MessageDate, Incoming, HasAttachment , Headers, CreatedDate, CreatedById, CreatedBy.Name, LastModifiedDate, LastModifiedById, LastModifiedBy.Name, ReplyToEmailMessageId, IsExternallyVisible, ActivityId 
                                   FROM EmailMessage WHERE ActivityId = '00T9000003212fs'];
        
        List<Account> ac =[Select id from Account];
    
System.debug('!!! '+msgs.size()); 
        System.debug('!!! '+ac.size()); 
        //System.assertEquals(1, msgs.size());
    
    }
    
    static testmethod void TestEmailMessage2() {
        
        Account a = new Account();
        a.Name = 'Acme Inc';
        
        insert a;
        
        Task tk = new Task();
        tk.WhatId = a.Id;
        tk.Status = 'Open';
       
        tk.Subject = 'Test Email';
        
        insert tk;
        
        List<Task> ts = [SELECT Id, Subject FROM Task WHERE Id = :tk.Id];
        System.assertEquals(1, ts.size());
        
        EmailMessage[] newEmail = new EmailMessage[0];
        newEmail.add(new EmailMessage(FromAddress = 'test@abc.org', Incoming = false, ToAddress= 'bmurphy@angelmedflight.com', 
        Subject = 'Test email', TextBody = '23456 ', ActivityId = tk.Id)); 
         
        //insert newEmail;
        
        List<EmailMessage> msgs = [SELECT Id, ParentId, FromAddress, ToAddress, CcAddress, BccAddress, FromName, Subject, TextBody, HtmlBody, Status, 
                    MessageDate, Incoming, HasAttachment , Headers, CreatedDate, CreatedById, CreatedBy.Name, LastModifiedDate, LastModifiedById, LastModifiedBy.Name, 
                    ReplyToEmailMessageId, IsExternallyVisible, ActivityId FROM EmailMessage];
    
        // WHERE ActivityId = :tk.Id
        System.Debug('@@@@@@@@@@@@@@@@ Size of messages: ' + string.valueOf(msgs.size()));
        System.assertEquals(1, msgs.size());
    }
    
}