public with sharing class sample {
  

    //List<dependncs__c> rt = [select state__c,city__c from dependncs__c];
    //for(dependncs__c na : rt)
    //op1.add(new selectoption('na','na'));
  
    public List<selectoption> getCities() {
    
    /*List<dependncs__c> rt = [select city__c from dependncs__c];
    List<selectoption> op= new  List<selectoption>();
     string nb;
    for(dependncs__c a : rt){
       nb=a.city__c;
    op.add(new selectoption(nb,nb));
    }
    return op;
    }*/
    
        List<selectoption> op1= new List<selectoption> ();
        if(state == 'AP')
        {
        List<dependncs__c> rt = [select city__c from dependncs__c where city__c=:'chen'];
         string nb;
         for(dependncs__c c : rt)
         nb=c.city__c;
        op1.add(new selectoption(nb,nb));
        }
       else if(state == 'TN')
       {
        List<dependncs__c> rt = [select city__c from dependncs__c where city__c=:'hyd'];
         string nc;
         for(dependncs__c c : rt)
         nc=c.city__c;
        op1.add(new selectoption(nc,nc));
       }
       else
        {
            op1.add(new SelectOption('None','--- None ---'));
        }      

        return op1;
    }


    public String city { get; set; }

    public  List<selectoption> getStates() {
       
       List<dependncs__c> rt = [select state__c from dependncs__c];
       List<selectoption> op= new  List<selectoption> ();
       op.add(new SelectOption('None','--- None ---'));
       string na;
       for(dependncs__c b:rt){
       na=b.state__c;
       op.add(new selectoption(na,na));
       }
       //op.add(new selectoption('None','None'));
       //op.add(new selectoption('AP','AP'));
       //op.add(new selectoption('TN','TN'));
         
        return op;
    }


    public String state { get; set; }
}