public class pnt_nw{
    //data members
    integer x1,y1;
     
    //membr methods
    //create
    //DC
    public pnt_nw(){
        system.debug('DC');
        x1=0;
        y1=0;
    }
     
    //PC
    public pnt_nw(integer x1,integer y1){
        //this();
        system.debug('PC');
        this.x1=x1;
        this.y1=y1;
      
    }
     
    //input
    public void setvalues(integer x1,integer y1){
         this.x1 = x1;
         this.y1 = y1;
    }
   
    //process
    public decimal slope(integer x2,integer y2){
        decimal sp;
        //system.debug('point is = ('+x2+','+y2+')');
        sp=(y2-y1)/(x2-x1); 
        return sp;
    }
     
    public decimal distance(integer x2,integer y2){
        decimal ds;
        ds = math.sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1));
       return ds;
    }

    //output
    public void getvalues(){
        //system.debug('the points are ('+x1+','+y1+')('+x2+','+y2+')');
        system.debug('point is = ('+x1+','+y1+')');
    }
 }