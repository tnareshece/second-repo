public class AddingItems {

    public String item { get; set; }

    public Boolean selected { get; set; }

    public String search { get; set; }
    public list<brand__C> brnLst {set;get;}

    public void search() {
        brnLst = new list<Brand__C>();
        list<brand__C> brnLst2 = new list <Brand__C>();
        brnLst2 = [SELECT id,name,Image__c,Price__c,ML__c FROM Brand__c WHERE name =: search];
        if(!brnLst2.isEmpty()){
            brnLst =  brnLst2 ;
        }
    }



    public PageReference toggleSort() {
        return null;
    }


    public String getBottles() {
        return null;
    }


    public PageReference runSearch() {
        return null;
    }

// Action method to handle purchasing process  
    
public PageReference addToCart() {
    /*for(brand__c p : brnLst ) {
        if(0 < p.quantity__c) {
            shoppingCart.add(p);
        }
    }*/
    return null; // stay on the same page  
    
}

public String getCartContents() {
    /*if(0 == shoppingCart.size()) {
        return '(empty)';
    }
    String msg = '<ul>\n';
    for(bottle__c p : shoppingCart) {
        msg += '<li>';
        msg += p.name + ' (' + p.quantity__c+ ')';
        msg += '</li>\n';
    }
    msg += '</ul>';*/
    //return msg;
     return null;
}


public class BrnWrapper{
 
      public brand__c tbrn {set;get;}
 
        //whether the record is selected
        public Boolean selected{get;set;}
 
        /*
        *   Constructor
        *   initializes the Contact reference
        */
        public BrnWrapper(brand__c c){
            this.tbrn = c;
            this.selected = false;
        }
 
    }

}