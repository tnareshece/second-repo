public with sharing class Bottle {

public Order myOrder {get;set;}
public list<wOrderItem> wOrderItems {get;set;}
    public Bottle  (){
         myOrder=new Order();
         wOrderItems = new list<wOrderItem>();
         if (ApexPages.currentPage().getParameters().get('id')!=null)
                {myOrder=[SELECT o.Type, 
                                 
                                 o.TotalAmount, 
                                 o.SystemModstamp, 
                                
                                 o.StatusCode, 
                                 o.Status, 
                                 
                                 
                                 o.ShippingStreet, 
                                 o.ShippingStateCode, 
                                 o.ShippingState, 
                                 o.ShippingPostalCode, 
                                 o.ShippingLongitude, 
                                 o.ShippingLatitude, 
                                 o.ShippingCountryCode, 
                                 o.ShippingCountry, 
                                 o.ShippingCity, 
                                 o.ShippingAddress, 
                                
                                 o.ShipToContactId, 
                                 ShipToContact.Name,
                                 ShipToContact.Phone,
                                 ShipToContact.Email,
                                 
                                 
                                 o.Pricebook2Id, 
                                 
                                 o.OriginalOrderId, 
                                 
                                 o.OrderNumber, 
                                 
                                 o.IsReductionOrder, 
                                 
                                 o.Id, 
                                 
                                 o.EndDate, 
                               
                                 o.EffectiveDate, 
                                 o.Description, 
                                 
                                 o.CustomerAuthorizedById, 
                                 
                                  
                                 o.CreatedDate, 
                                 o.CreatedById, 
                                 o.ContractId, 
                                 
                                 
                                 o.BillingStreet, 
                                 o.BillingStateCode, 
                                 o.BillingState, 
                                 o.BillingPostalCode, 
                                 o.BillingLongitude, 
                                 o.BillingLatitude, 
                                 o.BillingCountryCode, 
                                 o.BillingCountry, 
                                 o.BillingCity, 
                                 o.BillingAddress, 
                                 o.BillToContactId, 
                                 BillToContact.Name,
                                 BillToContact.Phone,
                                 BillToContact.Email,
                                 o.ActivatedDate, 
                                 o.AccountId,
                                
                                 o.PoNumber,
                                 Account.Name, 
                                
                                     (SELECT Id, 
                                             OrderId, 
                                             PricebookEntryId,
                                             PriceBookEntry.Product2.Name,
                                             PriceBookEntry.Product2.ProductCode,
                                             PriceBookEntry.Product2.Description,
                                             
                                             OriginalOrderItemId, 
                                             AvailableQuantity, 
                                             Quantity,
                                             
                                             UnitPrice, 
                                             ListPrice, 
                                             ServiceDate, 
                                             EndDate, 
                                             Description, 
                                             CreatedDate, 
                                             CreatedById, 
                                             LastModifiedDate, 
                                             LastModifiedById, 
                                             SystemModstamp, 
                                             OrderItemNumber
                                        
                                      FROM OrderItems 
                                      WHERE IsDeleted=false                                                 
                                      ORDER BY PriceBookEntry.Product2.ProductCode)
                                      
                         FROM Order o
                         WHERE o.Id=:ApexPages.currentPage().getParameters().get('id')];


 
//use loop to create wOrderItem
                 
                 if (myOrder.OrderItems.size()>0){
                     string UniqueValue=myOrder.OrderItems[0].PriceBookEntry.Product2.ProductCode;
                     string Product=myOrder.OrderItems[0].PriceBookEntry.Product2.Name;
                     string ProductCode=myOrder.OrderItems[0].PriceBookEntry.Product2.ProductCode;
                     string Description=myOrder.OrderItems[0].PriceBookEntry.Product2.Description;
                    
                     for (OrderItem myItem : myOrder.OrderItems){
                         if (myItem.PricebookEntry.Product2.ProductCode!=UniqueValue){
                             wOrderItems.add(new wOrderItem(
                             Product,
                             ProductCode,
                             
                             Description
                           
                             ));
                             UniqueValue=myItem.PriceBookEntry.Product2.ProductCode;
                             
                             Product=myItem.PriceBookEntry.Product2.Name;
                             ProductCode=myItem.PriceBookEntry.Product2.ProductCode;
                             Description=myItem.PriceBookEntry.Product2.Description;
                             
                         }
                        
                     }
                     wOrderItems.add(new wOrderItem(
                             Product,
                             ProductCode,
                            
                             Description
                            
                                 ));
                 }
         }

    }//end of constructor
//////////////////////////////////////////////////////////////
//Wrapper for Order Item agreegate query list
    
    
    private class wOrderItem{
        public string Product {get;set;}
        public string ProductCode {get;set;}
        
        public string Description {get;set;}
        
        //This is the contructor method.
        public wOrderItem(string myProduct, string myProductCode, string myDescription) {
            Product=myProduct;
            ProductCode=myProductCode;
            
            Description=myDescription;
            
        }
    }//end of class

}//end of Controller