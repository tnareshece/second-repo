public with sharing class OpportunityTriggerHandler_New {
     
      public static void onAfterInsertUpdate(map<ID,Opportunity> newmap,map<ID,Opportunity> oldmap,Boolean isInsert)
      {
     
        
          set<ID> accIDs = new set<ID>();
          set<ID> ownerIDs = new set<ID>();     
          
         system.debug('---newmap values---'+newmap.values());
         
          for(Opportunity oppObj : newmap.values()){
              if(oppObj.AccountID != null){
                  accIDs.add(oppObj.AccountID);
                  ownerIDs.add(oppObj.OwnerID);
              }
          }
          system.debug('---accIDs---'+accIDs);
          system.debug('---ownerIDs---'+ownerIDs);
          
          map<ID,User> ownerMap = new map<ID,User>([SELECT ID FROM User WHERE ID IN: ownerIDs]);
          system.debug('---ownerMap---'+ownerMap);
          map<ID,Account> accountMap = new map<ID,Account>([SELECT ID,OwnerID FROM Account WHERE ID IN: accIDs]);
          system.debug('---accountMap---'+accountMap);
          
          map<ID,String> oppTeam = new map<ID,String>();
          map<ID,String> oppTeamRole = new map<ID,String>(); 
         
         ObjectPermissions obj=[SELECT Id,ParentId,PermissionsCreate,PermissionsDelete,PermissionsEdit,PermissionsModifyAllRecords,PermissionsRead,PermissionsViewAllRecords,SobjectType FROM ObjectPermissions WHERE SobjectType = 'opportunity' AND ParentId IN (select id from permissionset where
         PermissionSet.Profile.Id =:userinfo.getProfileId()) limit 1];
         system.debug('-----obj----'+obj);
        
                     
         for(Opportunity oppObj2 : newmap.values()){
              if(oppObj2.AccountID != null && accountMap.containsKey(oppObj2.AccountID) && accountMap.get(oppObj2.AccountID).OwnerID != oppObj2.OwnerID){
                      if(isInsert){
                      oppTeam.put(accountMap.get(oppObj2.AccountID).OwnerID,oppObj2.ID);
                      oppTeamRole.put(oppObj2.ID,'Account Manager'); 
                      //oppTeamRole.put(oppObj2.ID,ownerMap.get(oppObj2.OwnerID).Role__c);         
                                 
                      }
                      else{
                      system.debug('-----obj----'+obj.PermissionsModifyAllRecords);
                      if(obj.PermissionsModifyAllRecords){
                      oppTeam.put(oppObj2.OwnerID,oppObj2.ID);
                      //oppTeamRole.put(oppObj2.ID,ownerMap.get(oppObj2.OwnerID).Role__c); 
                      oppTeamRole.put(oppObj2.ID,'Account Manager'); 
                      }
                      }
                      
              }
          }  
         
          if(!oppTeam.isEmpty() && !oppTeamRole.isEmpty()){
          system.debug('-----hello----');
          TeamMemberUtil_New.OpportunityTeamMembers(oppTeam,oppTeamRole); 
          }
                    
      }
      
      
  }