public with sharing class BankAcntlogin {

    public Integer ta { get; set; }

    public String an { get; set; }

    public String nam { get; set; }

    public decimal wd { get; set; }
    
    public String pin { get; set; }

    public String acntnum { get; set; }
    
    public decimal bl { get; set; }
    
    public String name { get; set; }
    
    public decimal baln { get; set; }
    
    public string cust_nam { get; set; }
    
    
    public PageReference actn() {
    
         bankaccount__c b =[select balance__c from bankaccount__c WHERE accountnum__c=:acntnum];
         //bankaccount__c d =[select accountnum__c from bankaccount__c];
          if(an=='a-0001' || an=='a-0002'|| an=='a-0003'){
          b.balance__c-=ta;
          update b;
          
          bankaccount__c c =[select balance__c from bankaccount__c WHERE accountnum__c=:an];
          c.balance__c+=ta;
          update c;
          }
        return null;
    }



    public PageReference sbmt() {
        
        bankaccount__c b =[select balance__c from bankaccount__c WHERE accountnum__c=:acntnum];
        //for(bankaccount__c b : shw)
        //if(b.accountnum__c==acntnum){
        b.balance__c-=wd;
        update b;
    
        return null;
    }


   
    public PageReference login() {
        
       List<BankAccount__c> ba = [select name,accountnum__c,pin__c,balance__c from bankaccount__c];
        
       for(bankaccount__c b : ba){
         if(acntnum==b.accountnum__c && integer.valueOf(pin)==b.pin__c)
         {
          //baln=b.balance__c;
          bl=b.balance__c;
          nam=b.name;
          return page.BankAcntpg2;
         }
        else{
          ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Invalid Credentials'));
        } 
      }
        return null; 
    }
    
  
    
    /*public PageReference shwbal() {
        
        bankaccount__c b =[select balance__c from bankaccount__c WHERE accountnum__c=:acntnum];
        //for(bankaccount__c b : shw)
        //if(b.accountnum__c==acntnum){
        bl=b.balance__c;
        //}
        return null;
    }*/


    

    public PageReference tran() {
        return page.BankAcntpg5;
    }


    public PageReference wtd() {
    
        return page.BankAcntpg4 ;
    }


    public PageReference bal() {
    
        return page.BankAcntpg3;
        
    }
 }