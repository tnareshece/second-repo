public class TriggerMailer_CC_75 { 
public static void sendEmail(String toAddress, String questionId, String errorMessage, String stackTrace) { 
String subjectText = 'Case Escalation exception in site ' + Site.getName(); 
String bodyText = 'Case Escalation on Question having ID: ' + questionId + ' has failed with the following message: ' + errorMessage + 
'\n\nStacktrace: ' + stackTrace; 

Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
String[] toAddresses = new String[] { toAddress }; 
mail.setReplyTo('no-reply@salesforce.com'); 
mail.setSenderDisplayName('Salesforce Chatter Answers User'); 

mail.setToAddresses(toAddresses); 
mail.setSubject(subjectText); 
mail.setPlainTextBody(bodyText); 
Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
} 
}