public virtual class BankAcntPrtException extends Exception {
     //data mem
     Protected Integer account_num,balance,deposit,withdraw;
     Protected String customer_name;
     
     //mem meth
     //create
     //PC
     public BankAcntPrtException(Integer account_num, Integer balance, String customer_name){
        this.account_num= account_num;
        this.balance=balance;
        this.customer_name=customer_name;
     }
     
     //input
     public void setValues(Integer account_num, Integer balance, String customer_name){
        this.account_num= account_num;
        this.balance=balance;
        this.customer_name=customer_name;
        }
            
     //process
     public void deposit(Integer dep){
        deposit=dep;
        balance+=dep;
     }
    
     public virtual void withdraw(Integer Wtd){
        withdraw=wtd;
        System.debug('withdr amnt is = '+withdraw);
        try{
            if( balance<=withdraw){
                 throw new BankAcntPrtException();
             }
        else{
              balance-=withdraw;
              System.debug('available balnce is = '+balance);
             }
           } 
        catch(BankAcntPrtException e){
               System.debug('Insufficient Balance');
              }
     }
     
      public void showbalance(){
        System.debug('The acnt balnce is ='+balance);
      }
    
      public virtual void display(){
        System.debug('Account Number =' +account_num); 
        System.debug('Balance =' +balance); 
        System.debug('Customer Name =' +customer_name); 
      }
    
}