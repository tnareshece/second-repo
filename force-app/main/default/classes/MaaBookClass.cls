public with sharing class MaaBookClass {

    public list<book__c> books;
    
    public list<book__c> getbooks(){
    books = [select id,name,author__C,edition__c,bookprice__c from book__C];
    return books;
    }
    }