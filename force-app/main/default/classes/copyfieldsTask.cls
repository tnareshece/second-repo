public with sharing class copyfieldsTask {

    public Integer phone2 { get; set; }

    public String name2 { get; set; }

    public Integer phone1 { get; set; }

    public String name1 { get; set; }
    
    public boolean checkbox { get; set; }
    
    public copyfieldsTask(){
    
    name1=name2=null;
    phone1=phone2=null;
    }

    public PageReference copyfields() {
    
        if(checkbox==true){
        name2=name1;
        phone2=phone1;
        }
        else{
        name2=null;
        phone2=null;
        }
        
        
        return null;
    }

}