public with sharing class MassDelete {

    public PageReference MassDel() {
        List<bottle__c> Mass = [select name,price__c,quantity__c from bottle__c];
        
        for(bottle__c m : Mass){
        delete m;
        }
        PageReference pr = new PageReference('https://ap1.salesforce.com/a0H/o');
        
        return pr;
    }

}