public with sharing class datalist2 {

    public boolean pass  { get; set; }
    public boolean pass2 { get; set; }
    public boolean pass3 { get; set; }
    public id id1 { get; set;} 
    public List<account> accounts { set; }
    public List<account> accountsList { get; set; }
    
    public datalist2(){
      pass2=true;
      pass=false;
      pass3=false;
    }
     
    public PageReference save() {
        update accountsList;
        pass=true;
        pass2=false;
        pass3=false;
        return null;
    }
    
     public PageReference Cancel() {
        if(pass==true){
        pass2=true;
        pass=false;
        }
        else if(pass3==true){
        pass=true;
        pass3=false;
        }
        return null;
    }
  
   public PageReference edit() {
        pass2=false;
        pass=false;
        pass3=true;
        accountsList = [SELECT Name,phone FROM Account WHERE id=:id1];
        return null;
    }
    
    public PageReference clik() {
        pass=true;
        pass2=false;
        pass3=false;
        System.debug('@@@@&&&&&&&***id is*@@@@@@*******'+id1);
        accountsList = [SELECT Name,phone FROM Account WHERE id=:id1];
         return null;
     }

    public List<Account> getAccounts() {
        List<account> accounts = [SELECT id,Name,phone FROM Account];
       return accounts;
    }
}