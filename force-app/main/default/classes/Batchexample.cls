Global class Batchexample implements database.Batchable<sobject>{
    global Database.QueryLocator start(Database.BatchableContext BC){
        
        String query = 'Select id,Name FROM Account';
        return Database.getQueryLocator(query);
    }
        
    
    global void execute(Database.BatchableContext BC, List<Account> accList){
        
        string str = 'Test';
        for(Account acc : accList){
         acc.Name = acc.Name.removeEnd('Test') ;   
        }
        
        try{
            update accList;
        }catch(Exception e){
            System.debug(e);
        }
    }
	
    global void finish(Database.BatchableContext BC){
        
    }
}