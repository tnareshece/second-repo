global class BatchProcessAccount implements Database.Batchable<sObject>{
    String query;
   
    global Database.querylocator start(Database.BatchableContext BC){
        Query = 'Select id,name,AccountNumber,type from account'; 
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<account> scope){
       List<Account> AccountList = new List<Account>();
       for(account acc : scope){
           acc.AccountNumber= '8888';
           AccountList.add(acc);
       }
       update AccountList ;
    }
   
    global void finish(Database.BatchableContext BC){
    }
}