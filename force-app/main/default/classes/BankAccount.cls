public class BankAccount{
//data members
    Integer accountnumber;
    Integer balance;
    String customername;
    Integer deposit;
    Integer withdraw;
    //static Integer count=0;
    //member methods
    //create
    //default constructer
    public BankAccount(){
        accountnumber=0;
        balance=0;
        customername='no name';
    }
    
    //parameterised constructer{
    public BankAccount(Integer acnbr,  Integer bal, String cname){
        
        accountnumber=acnbr;
        balance=bal;
        customername=cname;
        //count++;
     }
    //input
  public void setValues(Integer acnbr,  Integer bal, String cname){
   		accountnumber=acnbr;
        balance=bal;
        customername=cname;
    	//count++;
}
    //process
    // write deposit method
    public void deposit(Integer dep){
        deposit=dep;
        balance+=deposit;
    }
    // write withdraw method
    public void withdraw(Integer wdrw){
       withdraw=wdrw;
          balance-=withdraw;  
    }
    public void checkbalance(){
        system.debug('the account balance is ='+balance);
    }
    //output
    public void getValues(){
        system.debug('customer name='+customername);
        system.debug('account number='+accountnumber);
        system.debug('balance='+balance);
       }
}
    //end of the class