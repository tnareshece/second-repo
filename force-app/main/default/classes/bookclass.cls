public with sharing class bookclass {
     
 //default constructer
 public bookclass(){
      peru5=' ';
      HCL='RX';
      dhara=null;
      raasinodu=' ';
      edition=null;
     }
    
 public String peru { get; set; }
     
 public decimal dhara { get; set; }
     
 public String raasinodu { get; set; }
     
 public decimal edition { get; set; }
     
 
 public PageReference save() {
    
     //inserting record to model
     book__c bk = new book__c();
     bk.name=peru;
     bk.bookprice__C=dhara;
     bk.author__C=raasinodu;
     bk.edition__C=edition;
     insert bk;
     
     //redirctng to detaild page
    pagereference pr = new pagereference('/apex/pagename');
 
     pr.setredirect(true);
     return pr;
        
    }


 public PageReference saveandnew() {
    
     //inserting record to model
     book__c bk = new book__c();
     bk.name=peru;
     bk.bookprice__C=dhara;
     bk.author__C=raasinodu;
     bk.edition__C=edition;
     insert bk;
     
     //new page
     peru=' ';
     dhara=null;
     raasinodu=' ';
     edition=null;
        return null;
    }

 public PageReference cancel() {
     
     //redirctng to setup
     pagereference pr = new pagereference('https://ap1.salesforce.com/a0G/o');
     pr.setredirect(true);
     return pr;
   
    }

}