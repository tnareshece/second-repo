trigger Lead_Update on Lead (before update) {
    
    if(trigger.isBefore)
    {
        for(lead a:trigger.new)
        {
            If ( a.Division_Region__c == 'Americans' )
             {
                       If (a.Product_Interest__c != null)
                      {
                          if ( a.Industry == 'Accident Reconstruction'||a.Industry =='Forensics')
                             {
                               a.Product_Family_Txt__c = 'F04';
                             }
                          else if (a.Product_Interest__c.contains('Faro Arm') || a.Product_Interest__c.contains('Design ScanArm') || a.Product_Interest__c.contains('SCANARM')|| 
                                   a.Product_Interest__c.contains('Software-Metrology')||a.Product_Interest__c.contains('Robo-Imager Fixed') || a.Product_Interest__c.contains('Robo-Imager Mobile'))
                               {
                                a.Product_Family_Txt__c ='F01';
                               }       
                          
                          else if (a.Product_Interest__c.contains('Software - Law Enforcement') || a.Product_Interest__c.contains('Software – Insurance')||a.Product_Interest__c.contains('Software – Fire Service'))
                               {
                                a.Product_Family_Txt__c = 'F06';
                               }

                          else if (a.Product_Interest__c == 'Laser Tracker')  
                                {
                                    a.Product_Family_Txt__c = 'F02';
                                }
 
                          else if (a.Product_Interest__c.contains('Scan Localizer') || a.Product_Interest__c.contains('Freestyle3D'))
                                {
                                    a.Product_Family_Txt__c = 'F03';
                                }
                          else
                                {
                                    a.Product_Family_Txt__c = 'F01';
                                }
                      }
            }
            
    }
 }
}