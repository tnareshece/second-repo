trigger UpdateonCont on Task (after insert, after update) {
List<Id> contact_ids = new List<Id>(); 
List<Contact> update_contacts = new List<Contact>(); 
List<Contact> clist = new List<Contact>(); 

for(Task t : trigger.new){
//if the whoid is a contact and the alumni status is not null
if(t.Alumni__c!= Null){
contact_ids.add(t.WhoId);
}
}
clist=[SELECT Contacted_Once__c FROM Contact WHERE ID IN :contact_ids];
//if the id list is not empty
if(!contact_ids.isEmpty()){
//loop through associated contacts with completed tasks 
for(Contact c : clist){
//do updates here and add to list for dml
c.Contacted_Once__c = true;
update_contacts.add(c);
}
}

//do dml 
update update_contacts; 
}