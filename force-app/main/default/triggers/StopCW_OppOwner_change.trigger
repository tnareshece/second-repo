trigger StopCW_OppOwner_change on Account (before update) {

 Map<ID,ID>mapsid=new Map<ID,ID>();
 if(trigger.isbefore){
  for(opportunity op : [select id,stagename,OwnerId,accountid from opportunity where stagename = 'closed won' AND Accountid In: trigger.newMap.keySet()]){
     mapsid.put(op.id,op.ownerid);
 }
  Opp_ownerReminder_controller.addOPP(mapsid);
 }  

 }