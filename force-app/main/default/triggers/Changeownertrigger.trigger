trigger Changeownertrigger on Account (after insert,after update) {

      Set<Id> accountIds = new Set<Id>();

      Map<Id, String> oldOwnerIds = new Map<Id, String>();

      Map<Id, String> newOwnerIds = new Map<Id, String>();

      List<Opportunity> oppUpdates = new  List<Opportunity>();

      

      for (Account a : Trigger.new)

      {

         if (a.OwnerId != Trigger.oldMap.get(a.Id).OwnerId)

         {

            oldOwnerIds.put(a.Id, Trigger.oldMap.get(a.Id).OwnerId);

            newOwnerIds.put(a.Id, a.OwnerId);

            accountIds.add(a.Id);

         }

      }

        if (!accountIds.isEmpty()) {

         for (Account acc : [SELECT Id, (SELECT Id, OwnerId FROM Opportunities) FROM Account WHERE Id in :accountIds])

            {

            String newOwnerId = newOwnerIds.get(acc.Id);

            String oldOwnerId = oldOwnerIds.get(acc.Id);

            for (Opportunity o : acc.Opportunities)

            {

               if (o.OwnerId != oldOwnerId)

               {

               Opportunity updatedopp = new Opportunity(Id = o.Id, OwnerId = oldOwnerId);

               oppUpdates.add(updatedopp);

               }

            }

             

            }
       }

            update oppUpdates;

}