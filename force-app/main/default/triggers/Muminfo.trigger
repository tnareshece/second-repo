trigger Muminfo on Dummy__c (before insert) {
/*
trigger MuminFormatonTrigger on Mum_Information__c (before insert,before update) {
    if(trigger.Isbefore && trigger.isInsert){
        List<string> moblst = new List<string>();
        id uid = UserInfo.getUserId();
        list<Educator__c> edulist = [select id,hospital_name__r.id,city__r.id from Educator__c where User__c=:uid];
        for(Mum_Information__c mum :trigger.new){        
            moblst.add(mum.Mobile_No__c);
        }
        list<Mum_Information__c > mumList=new list<Mum_Information__c >();
        mumList=[select id,Mobile_No__c from Mum_Information__c where  Mobile_No__c in :moblst ];
        set<string> existMobile = new set<string>();
        for(Mum_Information__c  mum:mumList)
        {
            existMobile.add(mum.Mobile_No__c );
        }   
        for(Mum_Information__c mum :trigger.new){        
            if(existMobile.contains(mum.Mobile_No__c )){
                mum.Mobile_No__c.addError('Mobile number exists, please use new one');
            }
            if(mum.EduMum__c==null){
                mum.EduMum__c=edulist[0].id;
            }
            if(!mum.Bulk_Upload__c){
                mum.Interaction_Date__c=System.today();
            }
            //else{
            //    mum.Interaction_Date__c=NULL;
           // }
            if(mum.Days_Difference__c > -90 && mum.Days_Difference__c < 0){
                mum.Lifestage_Months__c = '3rd Trimester';
            }
            else if(mum.Days_Difference__c >= 0 && mum.Days_Difference__c <= 90){
                mum.Lifestage_Months__c = '< 3 months';
            }
            else if(mum.Days_Difference__c > 90 && mum.Days_Difference__c <= 365){
                mum.Lifestage_Months__c = '> 3 months';
            }
            if(mum.Mum_Hospital__c==null){
                 mum.Mum_Hospital__c=edulist[0].hospital_name__r.id;
             }
        }
    }  
    /////
    if(trigger.isbefore && (Trigger.isinsert || Trigger.isUpdate)){
         List<Mum_Information__c> mum = trigger.new;
         List<OTP_Generation__c> otp = [SELECT id, Genrated_OTP__c from OTP_Generation__c WHERE Mobile__c=:mum[0].Mobile_No__c AND Genrated_OTP__c=:mum[0].OTP__c];
        if(otp.size()>0) {
            mum[0].Validation_Stage__c='Validated';   
         } 
         else 
           mum[0].Validation_Stage__c='UnValidated';
    }
   /* if(trigger.isAfter && trigger.isInsert){
        list<integer> week = new list<integer>();
        list<integer> persona = new list<integer>();
        for(Mum_Information__c mum : trigger.new){
            week.add(integer.valueof(mum.current_week__c));
            persona.add(integer.valueof(mum.persona_mapping__c));
        }
        list<Message_Table__c> mesLst = [SELECT persona__c, week__c, name from Message_Table__c where persona__c IN: persona and week__c IN:week];
        system.debug('mesLst mesLst mesLst '+mesLst );
        system.debug('week week week '+week );
        system.debug('persona persona persona persona '+persona );
        list<Message_History__c> listmh = new list<Message_History__c>();
        for(Mum_Information__c mum : trigger.new)
        {
            for(Message_Table__c mt :mesLst )
            {
                if(mum.persona_mapping__c == mt.persona__c && mum.current_week__c == mt.week__c)
                {
                    Message_History__c mh = new Message_History__c();
                    mh.Phone__c=mum.mobile_no__c;
                    listmh.add(mh);
                }
             }
        }
    }   */
}