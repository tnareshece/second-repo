trigger TaskStatusUpdate on Lead (after update) {
/*set<id> leadids = new set<id>();
map<id,list<task>> leadtotaskmap = new map<id,list<task>>();
list<task> taskstoupdate = new list<task>();
for(lead Lead : trigger.new){
leadids.add(lead.id);
}

list<task> tasklist = [select whoid,status,ownerid from task where whoid in : leadids];

for(task t : tasklist){
if(leadtotaskmap.get(t.whoid)!=null){
list<task> temp = leadtotaskmap.get(t.whoid);
temp.add(t);
leadtotaskmap.put(t.whoid, temp);
}
else
{
leadtotaskmap.put(t.whoid, new list<task>{t});
}

}

for(lead lead : trigger.new){
if(lead.status=='Disqualified'){

list<Task> tasklisttemp = leadtotaskmap.get(lead.id);

for(task t : tasklisttemp){
if(t.ownerid=='00590000005joWi'){t.status='completed'; taskstoupdate.add(t);
}
}
   

}

}

if(taskstoupdate.size()>0) update taskstoupdate;*/
 Map<Id, Lead> leadMap = new Map<Id, Lead>([select id,Status from Lead where IsConverted = false AND id in:Trigger.newMap.keyset()]);
         
        Map<Id, Task> taskMap= new Map<Id, Task>([select whoid,status from task where STATUS != 'Completed' AND CreatedById = '005700000026H2Y' AND whoid in : Trigger.newMap.keyset()]);
        
        if(taskMap.size() > 0)
        {
            for(Task currentTask : taskMap.values())
            {
                if(leadMap.get(currentTask.whoid).Status=='Disqualified' || leadMap.get(currentTask.whoid).Status =='Return To Marketing')
                {
                    currentTask.status = 'Completed';
                    currentTask.Description = 'Auto-Closed by the SalesOps Trigger DisqStatusLeadTaskUpdate Disqualified or Return To Marketing Stage';
                }
                
                 else if(leadMap.get(currentTask.whoid).Status=='Nurture')
                {
                    currentTask.status = 'Completed';
                    currentTask.Description = 'Auto-Closed by the SalesOps Trigger DisqStatusLeadTaskUpdate Nurture Stage';
                }
           }
         
        update taskMap.values();
         
        }



}