trigger chatter_answers_question_escalation_to_case_trigger_CC_75 on Question (after update) { 
for (Question q: Trigger.new) { 
try { 
if (q.Priority == 'high' && (q.Cases == null || q.Cases.size() == 0) && Trigger.oldMap.get(q.id).Priority != 'high') { 
q = [select Id, Title, Body, CommunityId, createdById, createdBy.AccountId, createdBy.ContactId from Question where Id = :q.Id]; 
Case newCase = new Case(Origin='Chatter Answers', OwnerId=q.CreatedById, QuestionId=q.Id, CommunityId=q.CommunityId, Subject=q.Title, Description = (q.Body == null? null: q.Body.stripHtmlTags()), AccountId=q.CreatedBy.AccountId, ContactId=q.CreatedBy.ContactId); 
insert newCase; 
} 
} catch (Exception e) { 
TriggerMailer_CC_75.sendEmail(Site.getAdminEmail(), q.Id, e.getMessage(), e.getStacktraceString()); 
} 
} 
}