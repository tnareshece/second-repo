trigger LeadConvrt on Lead (before insert, after insert, after update) {

  
// no bulk processing; will only run from the UI
  
if (Trigger.new.size() == 1) {

    
if (Trigger.old[0].isConverted == false && Trigger.new[0].isConverted == true) {

      
// if a new account was created
      
if (Trigger.new[0].ConvertedAccountId != null) {

        
// update the converted account with some text from the lead
        
Account a = [Select a.Id, a.SLASerialNumber__c From Account a Where a.Id = :Trigger.new[0].ConvertedAccountId];
       
 a.SLASerialNumber__c = Trigger.new[0].SICCode__c;
        
update a;

      
}          

      
// if a new contact was created
      
if (Trigger.new[0].ConvertedContactId != null) {

       
 // update the converted contact with some text from the lead
       
 Contact c = [Select c.Id, c.Languages__c, c.Name From Contact c Where c.Id = :Trigger.new[0].ConvertedContactId];
        c.Languages__c = Trigger.new[0].SICCode__c;
        
update c;
}
}
}
}