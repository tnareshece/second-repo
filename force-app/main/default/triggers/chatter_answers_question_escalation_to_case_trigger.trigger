trigger chatter_answers_question_escalation_to_case_trigger on Question (after update) {
    for (Question q: Trigger.new) {
      
        try {
            if (q.Priority == 'high' && (q.Cases == null || q.Cases.size() == 0) && Trigger.oldMap.get(q.id).Priority != 'high') {
                q = [select Id, Title, Body, CommunityId, createdById, createdBy.AccountId, createdBy.ContactId from Question where Id = :q.Id];
                Case newCase = new Case(Origin = q.Origin,OwnerId=q.CreatedById, QuestionId=q.Id, CommunityId=q.CommunityId, Subject=q.Title, Description = (q.Body == null? null: q.Body.stripHtmlTags()), AccountId=q.CreatedBy.AccountId, ContactId=q.CreatedBy.ContactId);
                insert newCase;
            }
        } catch (Exception e) {
        
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setReplyTo('no-reply@salesforce.com');
            mail.setSenderDisplayName('Salesforce Chatter Answers User');

            // mail.setOrgWideEmailAddressId(orgWideEmailAddressId);
            mail.setToAddresses(new String[] { Site.getAdminEmail()});
           /*** Ihave changed setToAddresses manually from Site.getAdminEmail() to no-reply@salesforce.com ***/
           //mail.setToAddresses(new String[] {'no-reply@salesforce.com'});
            mail.setSubject('Case Escalation exception in site ' + Site.getName());
            mail.setPlainTextBody('Case Escalation on Question having ID: ' + q.Id + ' has failed with the following message: ' + e.getMessage() + '\n\nStacktrace: ' + e.getStacktraceString());
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                    }
    }
}