trigger contactupdate on account (after insert,after update) {
 Set<Id> accountIds = new Set<Id>();
    for(account ac : Trigger.new)
    {
         accountIds.add(ac.Id);//add ids of account 
    }
  Map<ID, Account> mapAccounts = new Map<ID, Account>([SELECT Id,Billing_City__c,(SELECT Id,Billing_City__c,AccountId FROM Contacts) FROM Account where Id IN :accountIds]);
  List<Account> lstAccToUpdate = new List<Account>();
  List<Contact> lstConToUpdate = new List<Contact>();
   for(account ac : Trigger.new)
   {
       Account acc = mapAccounts.get(ac.Id);
       lstAccToUpdate.add(acc);
       lstConToUpdate = [Select Billing_City__c from Contact where AccountID =: acc.Id];
        for(Contact con : lstConToUpdate)
        {
            con.Billing_City__c= acc.Billing_City__c;//assign phone of account to contact
        }   
 // lstConToUpdate.add(mapAccounts.get(opp.AccountId).Contacts);   
  }
 
 update lstConToUpdate;   
}